
# Procédure  

## Architecture :

Switch 1 (interface fa0/48) à Routeur 1 (interface gig0/0)

Switch 2 (interface fa0/48) à Routeur 2 (interface gig0/0)

Routeur 1 (interface gig0/1) à Routeur 2 (interface gig0/1)

## Switch :


Switching (Version : Switch 2960) : 

Nommer switch 
    
    Switch>enable
    Switch#conf t
    Switch#(config)#hostname S1
    S1(config)#exit
    S1#

Sécuriser avec un mot de passe sur switch :

    S1#conf t
    S1(config)#enable password peter  
    S1(config)#exit

Création des VLANs:

    S1#conf t
    S1(config)#vlan 2
    S1(config-vlan)#name Admin
    S1(config-vlan)#exit

    S1#conf t
    S1(config)#vlan 3
    S1(config-vlan)#name Comptabilite
    S1(config-vlan)#exit

    S1#conf t
    S1(config)#vlan 4
    S1(config-vlan)#name Production
    S1(config-vlan)#exit

Adressage VLANs : 

    S1#conf t
    S1(config)#interface vlan 2
    S1(config-if)#ip address 192.168.40.3 255.255.255.0 // attribuer une adresse IP au VLAN 2 afin de connaitre le réseau auquel il appartient : 192.168.40.0/24
    S1(config-if)#exit

    S1(config)#interface vlan 3
    S1(config-if)#ip address 192.168.50.3 255.255.255.0 // attribuer une adresse IP au VLAN 3 afin de connaitre le réseau auquel il appartient : 192.168.50.0/24
    S1(config-if)#exit

    S1(config)#interface vlan 4
    S1(config-if)#ip address 192.168.60.3 255.255.255.0 // attribuer une adresse IP au VLAN 4 afin de connaitre le réseau auquel il appartient : 192.168.60.0/24
    S1(config-if)#exit

Organisation interface vlan 2 :

    S1#conf t
    S1(config)#vlan 2
    S1(config-vlan)#interface range fa0/25 - 26
    S1(config-if-range)#switchport access vlan 2
    S1(config)#exit

Organisation interface vlan 3 :

    S1#conf t
    S1(config)#vlan 3
    S1(config-vlan)#interface range fa 0/1 - 12
    S1(config-if-range)#switchport access vlan 3
    S1(config)#exit

Organisation interface vlan 4 :

    S1#conf t
    S1(config)#vlan 4
    S1(config-vlan)#interface range fa 0/13 - 24
    S1(config-if-range)#switchport access vlan 4
    S1(config)#exit

Configuration trunk inter-vlan :

    S1#conf t
    S1(config)#interface fa 0/48 1 (Interface GE 0/0) à Routeur
    S1(config-if)#switchport mode trunk
    S1(config-if)#no shutdown
    S1(config-if)#exit

## Routeur : 

Nommer le routeur

    Router>enable
    Router#conf t
    Router(config)#hostname R1
    R1(config)#exit
    R1#

Sécuriser avec un mot de passe sur routeur :

    R1#conf t
    R1(config)#enable password peter   
    R1(config)#exit

Creation sous-interface VLAN 2 (ROUTEUR)

    R1#conf t
    R1(config)#interface gig 0/0.3
    R1(config-if)#encapsulation dot1Q 2   
    R1(config-subif)#ip address 192.168.40.254 255.255.255.0
    R1(config-subif)#ip helper-address 192.168.40.150
    R1(config-subif)#no shutdown
    R1(config-subif)#exit


Création sous-interface VLAN 3 (ROUTEUR)

    R1(config)#interface gig 0/0.1
    R1(config-if)#encapsulation dot1Q 3
    R1(config-subif)#ip address 192.168.50.254 255.255.255.0
    R1(config-subif)#ip helper-address 192.168.40.150
    R1(config-subif)#no shutdown
    R1(config-subif)#exit


Création sous-interface VLAN 4 (ROUTEUR)

    R1(config)#interface gig 0/0.2
    R1(config-if)#encapsulation dot1Q 4 
    R1(config-subif)#ip address 192.168.60.254 255.255.255.0
    R1(config-subif)#ip helper-address 192.168.40.150
    R1(config-subif)#no shutdown
    R1(config-subif)#exit


Activation interface gig0/0

    R1(config)#interface gig0/0 
    R1(config-subif)#ip helper-address 192.168.40.150 
    R1(config-subif)no shutdown  

Test :

    Ping inter vlan 


## Configuration table de routage  // procédure à faire sur le routeur 1

    R1#conf t
    R1(config)#router rip
    R1(config-router)#version 2 
    R1(config-router)#network 10.0.0.0
    R1(config-router)#network 192.168.40.0
    R1(config-router)#network 192.168.50.0
    R1(config-router)#network 192.168.60.0
    R1(config-router)#exit

###  Activation de l'interface gig0/1

    R1(config)#interface gig0/1
    R1(config-if)#ip address 10.0.0.1 255.0.0.0  
    R1(config-if)#no shutdown

## Configuration table de routage  // procédure à faire sur le routeur 2 (deuxième groupe)

    R2#conf t
    R2(config)#router rip 
    R2(config-router)#version 2
    R2(config-router)#network 10.0.0.0
    R2(config-router)#network 192.168.10.0
    R2(config-router)#network 192.168.20.0
    R2(config-router)#network 192.168.30.0
    R2(config-router)#exit

### Activation de l'interface gig0/1

    R2(config)#interface gig0/1
    R2(config-if)#ip address 10.0.0.2 255.0.0.0  
    R2(config-if)#no shutdown

## NAT

    Sur routeur 1 :

    R1#conf t
    R1(config)#ip nat inside source static 192.168.40.254 10.0.0.1 

### A faire sur chaque sous-interface Vlan:

    R1#conf t
    R1(config)#interface gig0/0.1
    R1(config-if)#ip nat inside 
    R1#(config-if)#exit
    
    R1#conf t
    R1(config)#interface gig0/0.2
    R1(config-if)#ip nat inside 
    R1#(config-if)#exit
    
    R1#conf t
    R1(config)#interface gig0/0.3
    R1(config-if)#ip nat inside 
    R1#(config-if)#exit

### A faire sur l'interface extérieur routeur :

    R1#conf t
    R1(config)#interface gig0/1
    R1(config-if)#ip nat outside
    R1(config-if)#exit