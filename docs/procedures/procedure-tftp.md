# Procedure TFTP

## Installation :

    apt-get install tftpd-hpa

## Fichier de configuration :

    nano /etc/default/tftpd-hpa


## Connexion au switch/routeur en SSH au préalable : 

    Putty

## Sauvegarder fichier configuration switch/routeur : 

    copy run tftp
    Address or name of remote host []? "IPv4 PC destination"
    Destination filename [switch-confg]? "écrire nom du fichier config"
    !!   
    7235 bytes copied in 0.880 secs (8222 bytes/sec)
    //confirmation de la création du fichier config nommé ci-dessus dans le repertoire "tftp"//

## Envoyer le fichier de configuration sur switch/routeur

    copy tftp run 
    Address or name of remote host []? "IP PC source du fichier config"
    Source filename []? "nom du fichier config concerné dans le repertoire TFTP" 

## Problème rencontré : 
### Error socket:


    Sur les PC:
        Ne pas oublier l'adressage : 
            "ip addr add "IPv4" dev "interface de notre réseau privé"
            "ip link set up dev "interface réseau privé"
        Créer la passerelle par défaut :
            "ip route add default via "IPv4 passerelle" dev "interface réseau privé"
--------------

    Récupération config switch:
        Créer un VLAN temporaire :
            "interface vlan 1"
            "ip address "IPv4" "masque"
            "no shutdown"

        Retourner sur la procédure TFTP ci-dessus
-------------
    Récupération routeur:
        Créer une passerelle sur un des ports du routeur (gig0/0 par exemple) :
            R1#conf t
            R1(config)#interface gig0/0
            R1(config-if)ip address "IPv4 passerelle" "masque"
            R1(config-if)#no shutdown


