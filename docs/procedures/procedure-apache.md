# Serveur WEB

## Installation Apache 2

    apt-get install apache 2

## Adressage ip en static : 

    nano /etc/network/interfaces

### rajout des lignes : 

    iface enp0s3 inet static 
        address 192.168.40.159/24
        gateway 192.168.40.254

## Acces au dossier page web : 

    cd /var/www/html/

// Tout ce qui sera intégré dans le dossier HTML est affiché sur la page web.

## exemple index.html :

    <!DOCTYPE html>


    <html>
	    <head>
		    <title> NetworkAndCo </title>
	    </head>
	    <body>
		    <h1> BIENVENUE </h1>
	    </body>
    </html>
