# Rapport de Projet Licence Pro CGIR
<img src="img/Logo-iut-lille1.svg" alt="drawing" width="200"/>

| Projet Network&Co                 |
|-----------------------------------|
| Création d'un Réseau d'Entreprise |
| Superviseur : Yvan Peter          |
| Étudiants : AL MASSATI Bilal, MARC Vianney, RENIER Antonin, LACHEREZ Hugo |  

# Remerciements

Nous tenons à d'abord remercier :

-   **Yvan Peter** et **Michael Hauspie** : Pour avoir aider et conseiller l'ensemble des équipes.

-   **L'IUT A de Lille** : Pour nous avoir fourni le matériel permettant la réalisation du projet.

# Présentation du projet

Notre projet comporte comme demande principale l‘installation d’une infrastructure réseau d’entreprise, il est composé d’une demande spécifique comme :

* Il doit comporter une partie publique, accessible de l'extérieur et une partie privée pour les machines utilisateurs.
* Chaque groupe devra héberger son serveur web et celui-ci devra être accessible aux autres groupes.
* Les utilisateurs pourront s'échanger des mails y compris d'un groupe à l'autre.
* La partie privé sera sécurisée par un pc sous Linux.
* Les adresses de la partie privée seront en adressage privé et ne devront pas être visibles à l’extérieur.
* Les machines de la partie privée appartiennent soit au département comptabilité soit au département production en sachant que les flux de ces différents départements devront être séparés pour assurer la qualité de service du réseau. Toutefois, une communication entre les machines des deux départements doit rester possible.
* De manière à faciliter la vie de l’administrateur et des utilisateurs, les machines utilisateur seront configurées dynamiquement. 
* Les différentes machines utilisateurs ainsi que les serveurs pourront être désignés par un nom.

Notre tuteur de projet a prévue des tâches en plus,si nous arrivons à terme du projet en avance, notamment :
* Fournir l’ensemble des services en IPv4 et en IPv6
* Offrir un accès sécurisé depuis un réseau extérieur (VPN).
* Offrir aux utilisateurs un accès Wifi. Le flux correspondant sera séparé   des autres flux et permettra uniquement le flux HTTP.

# Suivi du projet

## Matériels utilisés

Nous devons faire un inventaire du matériel que nous allons utiliser afin de pouvoir nous projeter.

* **Switch :** Celui-ci est un équipement qui permet à deux appareils informatiques ou plus, tels que des ordinateurs, de communiquer entre eux. La connexion de plusieurs appareils informatiques crée un réseau de communication.
* **Routeur :** Équipement réseau informatique assurant le routage des paquets. Son rôle est de faire transiter des paquets d'une interface réseau vers une autre au mieux, selon un ensemble de règles. 
* **PC :** Nombre de PC en fonction du nombre d'utilisateurs de notre parc informatique.
* **PC Serveur :** Ces PC serviront à héberger nos serveurs,notamment pour la gestion du DNS,DHCP ou bien du serveur Web.
* **Câbles Ethernet :** La prise Ethernet est une prise au format standardisé pour recevoir des signaux multimédias. Elle permet donc de transmettre de l’information. Elle est utilisée pour la connectique entre les diverses entités réseau (PC,switch…).

## Services utilisés

Afin de mettre en place notre réseau il faut sélectionner les différents services qui seront déployés sur notre infrastructure.

* **DNS :** Bind9 est un service TCP/IP permettant la correspondance entre un nom de domaine qualifié et une adresse IP.

<p align="center"> <img src="img/Bind_9_Mark_ISC_Blue.png" alt="drawing" width="75"/>

* **DHCP :** ISC-DHCP offre divers avantages, notamment la configuration d'adresse IP fiable. Le DHCP minimise les erreurs de configuration provoquées par la configuration manuelle des adresses IP. Mais aussi, il permet de donner automatiquement les paramètres réseau à une machine au démarrage du système.

<p align="center"> <img src="img/ISC-logo-rgb-2048x1149.png" alt="drawing" width="100"/>
</p>

* **Serveur Web :** Apache 2 est un serveur web. Son rôle est d'écouter les requêtes émises par les navigateurs (qui demandent des pages web), de chercher la page demandée et de la renvoyer. De plus, il est OpenSource et donc gratuit et libre de configuration.

<p align="center"> <img src="img/Apache_logo.svg.png" alt="drawing" width="150"/>
</p>

* **Serveur Mail :** Postfix est un serveur de messagerie STMP libre et répandu dans le domaine Linux. Postfix est assez simple dans son fonctionnement et sa configuration l'est aussi. Nous avons configuré le main.cf pour l'adapter à nos besoins. 

<p align="center"> <img src="img/Postfix_logo.png" alt="drawing" width="150"/>
</p>

## Outils de gestion de projet

### MSProject
 MSProject est un logiciel de gestion de projets Il permet la planification et le pilotage de projets, de gérer les ressources et le budget, ainsi que d'analyser et communiquer les données des projets.

<p align="center"> <img src="img/Microsoft_Project.svg.png" alt="drawing" width="90"/>
</p>

### Markdown

Markdown est un langage de balisage léger qui a pour but d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé.

<p align="center"> <img src="img/Markdown-mark.svg.png" alt="drawing" width="90"/>
</p>

### Draw.io

Draw.io est un outil en ligne gratuit permettant de réaliser différents types de diagrammes.

<p align="center"> <img src="img/DrawIO.png" alt="drawing" width="150"/>
</p>

### Git
<p align="center"> <img src="img/Gitlab-Logo.png" alt="drawing" width="150"/>
</p>

GitLab est une plateforme de développement collaborative open source. Elle permet de publier des dépôts de code source et de gérer leurs différentes versions.

Afin de gérer les différentes versions de nos fichiers et l'avancement du projet, nous avons crée un dépot Git pour chaque groupe et lui avons donné un nom d'entreprise fictif pour rester dans le thème du projet.

Pour y accéder et modifier depuis chez nous, nous avons installé Git Bash.

Voici les commandes Git les plus utilisées en général :

| Commande                                         | But                                                         |
|--------------------------------------------------|-------------------------------------------------------------|
| git clone "link"                                 | Clonage du dépot distant en local                           |
| git add "file"                                   | Ajout d'un fichier au dépot local                           |
| git commit -m "msg"                              | Attestation des changements au dépot                        |
| git push                                         | Chargement du dépot local au dépot distant                  | 

## Calendrier Hebdomadaire

### Semaine 1 (03/10/2022 - 07/10/2022)
* Découverte du projet et de notre cahier des charges
* Mise en place de la cohésion équipe
* Création du projet sur Gitlab :
    * Création d’un dépôt local
    * Ajout des fichiers au dépôt local
    * Création du groupe sur Gitlab

### Semaine 2 (10/10/2022 - 14/10/2022)
* Analyse du cahier des charges
* Recherche et échange sur les matériels et services à utiliser
* Simulation sur packet tracer
* Mise en place et attribution des tâches

### Semaine 3 (17/10/2022 – 21/10/2022)
* Configuration switch
* Configuration routeur
* Création VLAN
* Configuration INTER-VLAN
* Configuration INTER-ROUTAGE

### Semaine 4 (24/10/2022 – 30/10/2022)
* Création de machines virtuelles pour les différents services réseau sur Debian 11
* Continuité des configurations switch/routeur
* Suivi et écriture des procédures

### Semaine 5 ( 07/11/2022 – 13/11/2022)
* Application des configurations switch/routeur de packet tracer en réel
* Réglage des petits problèmes de compatibilité (entre packet tracer et la version de nos équipements)
* Branchement physique de notre infrastructure
* Configuration des PC utilisés pour les intégrer au réseau
* Installation et configuration du serveur TFTP afin de sauvegarder, d’importer et d’exporter nos configurations switch / routeur
* Début d’installation du DHCP et DNS sur les machines virtuelles
* Suivi et écriture des procédures

### Semaine 6 ( 14/11/2022 – 20/11/2022)
* Test de fonctionnement du réseau et des configurations routeur / Switch
* Configuration du DHCP et DNS
* Suivi et écriture des procédures

### Semaine 7 ( 21/11/2022 – 27-11/2022)
* Finalisation et test du fonctionnement du DHCP
* Configuration DNS
* Commencement de l'installation du Serveur WEB, Serveur mail
* Suivi et écriture des procédures, tâches faites

### Semaine 8 (28/11/2022 – 03/12/2022)
* Configuration DNS
* Configuration NAT pour l’accès extérieur
* Finition et test du serveur WEB
* Rédaction du rapport de projet
* Suivi et écriture des procédures

### Semaine 9 (05/12/2022 – 11/12/2022) 

* Résolution et configuration du DNS
* Test de mise en place d’infrastructure avec des PC
* Installation et configuration d'un Firewall
* Suivi et écriture des procédures

### Semaine 10 (12/12/2022 – 18/12/2022) 

* Validation et test du DNS
* Tentative résolution problème Firewall
    * Vue avec Monsieur Peter de ne pas utiliser la DMZ contenu de l'avancement de notre projet
* Documentation Serveur Mail
* Rédaction du rapport de projet

### Semaine 11 (02/01/2023 – 08/01/2023) : 
* Test et simulation finale infrastructure.
* Préparation soutenance orale.

# Schéma explicatif du réseau

<p align="center"> <img src="img/schema_projet.png" alt="drawing"/>
</p>

Voici le schéma des deux réseaux mis en place durant ce projet. Les ordinateurs de la partie privée sont en adressage privé, les interfaces extérieures des routeurs sont publiques.
Un pare-feu sous Linux se trouve sous chaque partie privée du routeur.
Chaque serveur contient un service web, mail, DHCP et DNS.

**Le switch contient 3 VLANs :**

* VLAN Comptabilité
* VLAN Production
* VLAN Administration (contenant les services)

Ces 3 VLANs peuvent communiquer entre eux grâce au routage inter-VLAN.
Ensuite les 2 réseaux sont reliés via un réseau public.
Ici, on se concentrera sur la partie droite (en rouge) du schéma.

# Mise en place et configuration de l'infrastructure

Nous devons faire un inventaire du matériel que nous allons utiliser afin de pouvoir nous projeter.

## Switch 

<p align="center"> <img src="img/cisco-catalyst-2960-48-Port.png" alt="drawing" width="200"/>
</p>

Celui-ci est un équipement qui permet à deux appareils informatiques ou plus, tels que des ordinateurs, de communiquer entre eux. La connexion de plusieurs appareils informatiques crée un réseau de communication.

Ici nous utiliserons le modèle **catalyst 2960** de la marque cisco.

Comme convenu dans le cahier des charges il faut séparer le réseau en trois VLANs :

* **VLAN 2 Administration** : 192.168.40.2/24
* **VLAN 3 Comptabilité** : 192.168.50.3/24
* **VLAN 4 Production** : 192.168.60.4/24

On a donc un fichier de configuration pour le switch avec l'attribution des différents ports aux VLANs

```
interface FastEthernet0/25
 switchport access vlan 2
!
interface FastEthernet0/26
 switchport access vlan 2
!
interface FastEthernet0/1
 switchport access vlan 3
!
interface FastEthernet0/2
 switchport access vlan 3
!
interface FastEthernet0/13
 switchport access vlan 4
!
interface FastEthernet0/14
 switchport access vlan 4
!
interface Vlan1
 no ip address
!
interface Vlan2
 ip address 192.168.40.3 255.255.255.0
!
interface Vlan3
 ip address 192.168.50.3 255.255.255.0
!
interface Vlan4
 ip address 192.168.60.3 255.255.255.0
!
```

## Routeur

<p align="center"> <img src="img/cisco-2911.png" alt="drawing" width="250"/>
</p>

Équipement réseau informatique assurant le routage des paquets. Son rôle est de faire transiter des paquets d'une interface réseau vers une autre selon un ensemble de règles.

Sur notre Routeur nous avons configuré un NAT qui va permettre la translation d'adresse pour qu'il soit accessible depuis l'extérieur.

Le routeur possède donc une adresse publique, une adresse privée et également trois sous interface pour les VLANs.


| Adresse IP Privée | Adresse IP Publique |
|-------------------|---------------------|
| 192.168.10.254    | 10.0.0.2            |

- **Gig0/0.1 (VLAN 2)** : 192.168.40.254/24
- **Gig0/0.2 (VLAN 3)** : 192.168.50.254/24
- **Gig0/0.3 (VLAN 4)** : 192.168.60.254/24
- **Gig0/1**            : 10.0.0.1/30


```
interface GigabitEthernet0/0.1
 encapsulation dot1Q 2
 ip address 192.168.40.254 255.255.255.0
 ip helper-address 192.168.40.150
 ip nat inside
 ip virtual-reassembly in
!
interface GigabitEthernet0/0.2
 encapsulation dot1Q 3
 ip address 192.168.50.254 255.255.255.0
 ip helper-address 192.168.40.150
 ip nat inside
 ip virtual-reassembly in
!
interface GigabitEthernet0/0.3
 encapsulation dot1Q 4
 ip address 192.168.60.254 255.255.255.0
 ip helper-address 192.168.40.150
 ip nat inside
 ip virtual-reassembly in
!
interface GigabitEthernet0/1
 ip address 10.0.0.1 255.0.0.0
 ip nat outside
 ip virtual-reassembly in
 shutdown
 duplex auto
 speed auto
!
interface GigabitEthernet0/2
 no ip address
 shutdown
 duplex auto
 speed auto
!
ip nat inside source static 192.168.40.254 10.0.0.1
!
```

Nous avons un serveur faisant office de DHCP qui va s'occuper de distribuer automatiquement les adresses IP aux PC des utilisateurs.

Étant donné que l'infrastructure présente des VLANs, les VLANs Comptabilité et Production n'avez pas accès au service DHCP. Pour cela nous avons trouvé une commande à ajouter dans le fichier de configuration du Routeur.

La ligne **ip helper-address 192.168.40.150** permet d'indique l'adresse du serveur DHCP au routeur pour que les VLANs Comptabilité et Production puissent fonctionner.

### Interconnexion entre les deux groupes

Toujours d'après le cahier des charges les deux réseaux devaient pouvoir communiquer entre eux. Nous avons donc ajouté une partie routage à la configuration de notre routeur.

La solution la plus optimale était d'utiliser un routage dynamique, nous nous sommes donc dirigé vers l'utilisation du protocole RIP

Le protocole RIP convient parfaitement aux petits réseaux. Avec celui-ci, un hôte passerelle envoie sa table de routage au routeur le plus proche toutes les 30 secondes. Ce routeur, à son tour, envoie le contenu de ses tables de routage aux routeurs voisins. De plus, les tables RIP sont limitées à 15 sauts.

```
router rip
 version 2
 network 10.0.0.0
 network 192.168.40.0
 network 192.168.50.0
 network 192.168.60.0
!
```

Grâce à ce protocole on obtient donc cette table de routage :

```
Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 2 subnets, 2 masks
C        10.0.0.0/8 is directly connected, GigabitEthernet0/1
L        10.0.0.1/32 is directly connected, GigabitEthernet0/1
      192.168.40.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.40.0/24 is directly connected, GigabitEthernet0/0.1
L        192.168.10.254/32 is directly connected, GigabitEthernet0/0.1
      192.168.50.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.50.0/24 is directly connected, GigabitEthernet0/0.2
L        192.168.50.254/32 is directly connected, GigabitEthernet0/0.2
      192.168.60.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.60.0/24 is directly connected, GigabitEthernet0/0.3
L        192.168.60.254/32 is directly connected, GigabitEthernet0/0.3
R     192.168.10.0/24 [120/1] via 10.0.0.2, 00:00:07, GigabitEthernet0/1
R     192.168.20.0/24 [120/1] via 10.0.0.2, 00:00:07, GigabitEthernet0/1
R     192.168.30.0/24 [120/1] via 10.0.0.2, 00:00:07, GigabitEthernet0/1

```

### Importation des configurations sur les matériels réseaux

Afin de ne pas bloquer les routeurs, les switchs et au cas ou les configurations seraient supprimées des différents équipements. Nous sauvergardions nos fichiers de configuration à distance et nous les ré-implémentions à chaque début de séance.

Pour cela nous avons utilisé le protocole TFTP.

```
Router#copy tftp: running-config
Address or name of remote host []? (host_ip)
Source filename []? backup_cfg_for_my_router
Destination filename [running-config]?
Accessing tftp://192.168.3.1/backup_cfg_for_my_router...
Loading backup_cfg_for_router from 192.168.3.1 (via FastEthernet0/0): !
[OK - 1030 bytes]

1030 bytes copied in 9.612 secs (107 bytes/sec)
R2#
```

# Pare-Feu

<p align="center"> <img src="img/firewall.png" alt="drawing" width="150"/>
</p>

Afin de sécuriser le réseau comme demandé, un pare-feu *IPTables* a été mis en place sur un PC sous Linux.

## Règles d'IPTables

Remise à zéro des règles de base : 

    iptables -F

On active le traffic en localhost

    iptables -A INPUT -i lo -j ACCEPT

    // -A --> pour préciser la chain
    // -i --> -i pour préciser l'interface
    // lo --> interface utilisé pour les communcations hôte local

Activation connexion HTTP/SSH/HTTPS

        iptables -A INPUT -p tcp --dport 22 -j ACCEPT
        // 22 pour le port SSH

        iptables -A INPUT -p tcp --dport 80 -j ACCEPT
        // 80 pour le port HTTP

        iptables -A INPUT -p tcp --dport 443 -j ACCEPT
        // 443 pour le port HTTPS

        // -p --> préciser le protocole
        // -dport --> préciser le port concerné

Activation connexion DNS

    iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT

    iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT

    iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT

    iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT


Activation pour serveur mail:


        iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT

        // 25 --> port 25 utilisé pour SMTP(envoie e-mail)

    ----

        iptables -t filter -A INPUT -p tcp --dport 110 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 110 -j ACCEPT

        // 110 --> port 110 utilisé pour POP3(reception d-email)

    -----

        iptables -t filter -A INPUT -p tcp --dport 143 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 143 -j ACCEPT

        // 143 --> port 143 utilisé pour IMAP( stockage des e-mail)

Autorisation PING :

        iptables -t filter -A INPUT -p icmp -j ACCEPT

        iptables -t filter -A OUTPUT -p icmp -j ACCEPT

        // icmp --> protocole de ping
        // - t filter --> table filter qui consiste a indiquer les régles des paquets,port,protocole.

Refuser tous les autres trafics sauf ceux autorisé :

    /!\ Attention: Cette commande est à appliquer en dernière régle sinon blocage de tout le reste./!\

        iptables -A INPUT -j DROP


Nous n'avons cependant pas intégré de pare-feu suite à notre choix de ne pas intégrer de *DMZ* étant donné l'avancement de notre projet. En effet en intégrer une reviendrait à revoir toute notre infrastructure et nos configuration.

Notre solution de subtitution est *ACL*.

# ACL
Les ACLs (Access Control List)  permettent de filtrer les accès entre les différents réseaux ou de filtrer les accès sur le routeur.

## Création de l'ACL
La création de l'ACL se fait sur le routeur.
```
R1# conf t
R1(config)# ip access-list extended exterieur // exterieur -> nom de l'acl
```

## Intégration des régles dans ACL
```
R1(config)#ip access-list extended exterieur

R1(config-ext-nacl)#permit tcp any 192.168.40.0 0.0.0.255 eq 53

    //-> Régle ACL d'autorisation du protocole TCP sur le port 53(DNS) de l'exterieur vers le réseau 192.168.40.0

R1(config-ext-nacl)#permit tcp any 192.168.40.0 0.0.0.255 eq 80

    //-> Régle ACL d'autorisation du protocole TCP sur le port 80(HTTP) de l'exterieur vers le réseau 192.168.40.0

R1(config-ext-nacl)#permit tcp any 192.168.40.0 0.0.0.255 eq 443

    //-> Régle ACL d'autorisation du protocole TCP sur le port 443(HTTPS) de l'exterieur vers le réseau 192.168.40.0

R1(config-ext-nacl)#permit udp any 192.168.40.0 0.0.0.255 eq 53

    //-> Régle ACL d'autorisation du protocole UDP sur le port 80(DNS) de l'exterieur vers le réseau 192.168.40.0

R1(config-ext-nacl)#permit udp any 192.168.40.0 0.0.0.255 eq 443

    //-> Régle ACL d'autorisation du protocole UDP sur le port 443(HTTPS) de l'exterieur vers le réseau 192.168.40.0

 R1(config-ext-nacl)#permit tcp any any eq 25

    //-> Régle ACL d'autorisation du protocole TCP sur le port 25(SMTP)

 R1(config-ext-nacl)#permit tcp any any eq 110

    //-> Régle ACL d'autorisation du protocole TCP sur le port 110(POP3)

 R1(config-ext-nacl)#permit tcp any any eq 143

    //-> Régle ACL d'autorisation du protocole TCP sur le port 143(IMAP)



R1(config-ext-nacl)#deny icmp any any

//->Regle ACL de filtrage du protocole ICMP de l'extérieur vers notre réseau privé.
```

## Application de ACL sur interface
```
R1#conf t
R1(config)#int GigaEthernet0/1
R1(config-if)#ip access-group exterieur out 
R1(config-if)end

//-> Attribution des règles ACL extérieur sur l'itnerface GigaEthernet0/1
```

### Table ACL
```
Extended IP access list exterieur
    10 permit tcp any 192.168.40.0 0.0.0.255 eq domain
    20 permit tcp any 192.168.40.0 0.0.0.255 eq www
    30 permit tcp any 192.168.40.0 0.0.0.255 eq 443
    40 permit udp any 192.168.40.0 0.0.0.255 eq domain
    50 permit udp any 192.168.40.0 0.0.0.255 eq 443
    60 deny icmp any any
    70 permit tcp any any eq smtp
    80 permit tcp any any eq pop3
    90 permit tcp any any eq 143

```

# DHCP

<p align="center"> <img src="img/ISC-logo-rgb-2048x1149.png" alt="drawing" width="200"/>
</p>

Pour notre DHCP nous avons décidé d'applique les plages d'adresses dynamiques suivantes. Nous avons décidé d'utiliser 20 adresses (de .30 à .50) car cela est suffisant pour notre infrastructure.

|        | Réseau          | Adresse Minimum | Adresse Maximum | Passerelle     |
|--------|-----------------|-----------------|-----------------|----------------|
| VLAN 2 | 192.168.40.0/24 | 192.168.40.30   | 192.168.40.50   | 192.168.40.254 |
| VLAN 3 | 192.168.50.0/24 | 192.168.50.30   | 192.168.50.50   | 192.168.50.254 |
| VLAN 4 | 192.168.60.0/24 | 192.168.60.30   | 192.168.60.50   | 192.168.60.254 |

Configuration du DHCP

```
default-lease-time 600;
max-lease-time 7200;
allow booting;
allow bootp;


ddns-update-style none;


server-identifier 192.168.40.150;
authoritative;
ddns-update-style interim;

#VLAN 2 

    subnet 192.168.40.0 netmask 255.255.255.0 {
        range  dynamic-bootp 192.168.40.20 192.168.40.100;
        option routers 192.168.40.254;https://gitlab.univ-lille.fr/hugo.lacherez.etu/networkandco.git
        option subnet-mask 255.255.255.0;
        option domain-name "networkandco.org";
        option domain-name-servers 192.168.40.151;
    }

#VLAN 3 

    subnet 192.168.50.0 netmask 255.255.255.0 {
        range  dynamic-bootp 192.168.50.20 192.168.50.100;
        option routers 192.168.50.254;
        option subnet-mask 255.255.255.0;
        option domain-name "networkandco.org";
        option domain-name-servers 192.168.40.151;
    }

#VLAN 4 

    subnet 192.168.60.0 netmask 255.255.255.0 {
        range  dynamic-bootp 192.168.60.20 192.168.60.100;
        option routers 192.168.60.254;
        option subnet-mask 255.255.255.0;
        option domain-name "networkandco.org";
        option domain-name-servers 192.168.40.151;
    }
-----
    host dns {
    hardware ethernet "MAC ADRESSE SERVEUR DNS"
    fixed-adress "IP DNS"
    }

    //PARTIE DNS DYNAMIQUE

    host dhcp {
    hardware ethernet "MAC ADRESSE SERVEUR dhcp"
    fixed-adress "IP dhcp"
    }
--------

    key "dhcp-update-key" {
        algorithm hmac-md5;
        secret "7R1U767pLKhR+O/0hF11TfeNmoVpVU+lfLnL3GhvWpI=";
    }

    zone networkandco.org {
    primary 192.168.40.151;
    key "rndc-key";
    }

    zone 40.168.192.in-addr-arpa
    {
    primary 192.168.40.151;
        key "rndc-key";
    }

    ddns-domainname "networkandco.org.";
    ddns-rev-domainname "in-addr.arpa.";


        



### Configuration dhcp inter-vlan:

#### Installation package inter-vlan :

    sudo apt-get update
    sudo apt-get install vlan

#### configuration fichier /etc/network/interfaces

     
    nano /etc/network/interfaces :
    
    # The primary network interface
    allow-hotplug enp0s3
    iface enp0s3 inet static
        address 192.168.40.150 //adresse ip static du serveur dhcp
        gateway 192.168.40.254


    #INTER VLAN

    #VLAN 2

    auto enp0s3
    iface Admin inet static
        address 192.168.40.3
        netmask 255.255.255.0
        gateway 192.168.40.254
    vlan-raw-device enp0s3

    #VLAN 3

    auto enp0s3
    iface Comptabilite inet static
        address 192.168.50.3
        netmask 255.255.255.0
        gateway 192.168.50.254
    vlan-raw-device enp0s3

    #VLAN 4

    auto enp0s3
    iface Production inet static
        address 192.168.60.3
        netmask 255.255.255.0
        gateway 192.168.60.254
    vlan-raw-device enp0s3
```

# DNS

<p align="center"> <img src="img/Bind_9_Mark_ISC_Blue.png" alt="drawing" width="100"/>

Le DNS va nous permettre d'accéder plus facilement au serveur web car il permet de ne pas avoir à entrer une adresse IP, il permet d'associer une adresse IP à un nom de domaine.
Ici nous avons utilisé *bind9* pour notre DNS 

## Configuration du DNS

/etc/bind/named.conf
```
// This is the primary configuration file for the BIND DNS server named.
//
// Please read /usr/share/doc/bind9/README.Debian.gz for information on the
// structure of BIND configuration files in Debian, *BEFORE* you customize
// this configuration file.
//
// If you are just adding zones, please do that in /etc/bind/named.conf.local
include "/etc/bind/named.conf.options";
include "/etc/bind/named.conf.local";
include "/etc/bind/named.conf.default-zones";
```

/etc/bind/named.conf.options
```
options {
allow-query { any; };
#allow-recursion { machine-locale; reseau-local; };
directory "/var/cache/bind/";
forward only;
forwarders { 192.168.40.254; };
// If there is a firewall between you and nameservers you want
// to talk to, you may need to fix the firewall to allow multiple
// ports to talk. See http://www.kb.cert.org/vuls/id/800113
// If your ISP provided one or more IP addresses for stable
// nameservers, you probably want to use them as forwarders.
// Uncomment the following block, and insert the addresses replacing
// the all-0's placeholder.
// forwarders {
// 192.168.40.254;
// };
// };
//=====================================================================>
// If BIND logs error messages about the root key being expired,
// you will need to update your keys. See https://www.isc.org/bind-keys
//=====================================================================>
dnssec-validation auto;
listen-on-v6 { none; };
};
```

/etc/bind/named.conf.local
```
//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";

acl machine-locale
{
127.0.0.1;
};
acl reseau-local
{
192.168.40.0/24;
};
zone "networkandco.org" IN {
type master;
file "/var/cache/bind/networkandco.org";
allow-update { key "rndc-key";};
};

zone "40.168.192.in-addr.arpa" {
type master;
notify no;
file "/var/cache/bind/networkandco.org.reverse";
allow-query { reseau-local; };
allow-update { key "rndc-key"; };
};

key "rndc-key" {
algorithm hmac-md5;
secret "7R1U767pLKhR+O/0hF11TfeNmoVpVU+lfLnL3GhvWpI=";
};
controls {
inet 127.0.0.1 port 953
allow { 127.0.0.1; } keys { "rndc-key"; };
inet 192.168.40.151 port 953 allow { 192.168.40.150; } keys { "rndc-key"; } ;
};
```

/var/cache/bind/networandco.org
```
$ORIGIN .
$TTL 604800 ; 1 week
networkandco.org IN SOA dns.networkandco.org. root.networkandco.org. (

20 ; serial
86400 ; refresh (1 day)
7200 ; retry (2 hours)
604800 ; expire (1 week)
3600 ; minimum (1 hour)
)

NS dns.networkandco.org.

$ORIGIN networkandco.org.
$TTL 300 ; 5 minutes
debian A 192.168.40.254

TXT "0027ebd2a577a13aaf9e6a00ff70687894"

$TTL 604800 ; 1 week
dhcp A 192.168.40.150
www A 192.168.40.159
dns A 192.168.40.151
parefeu A 192.168.40.155
$TTL 300 ; 5 minutes
pc A 192.168.50.34

TXT "31f82b6ec8da01136c738e8c5b993357be"

pc2 A 192.168.50.35

TXT "31c35e97c5ede00f0ddc1b2f831256b8db"

$TTL 604800 ; 1 week
```

/var/cache/bind/networkandco.org.reverse
```
;TTL d'une semaine
$TTL 604800
@ IN SOA 40.168.192.in-addr.arpa. root.networkandco.org. (

1 ; Serial
1D ; Refresh
2H ; Retry
7D ; Expire
1H ; Cache TTL

)
@ IN NS dns.networkandco.org.
151 IN PTR dns.networkandco.org.
150 IN PTR dhcp.networkandco.org.
159 IN PTR www.networkandco.org.
153 IN PTR smtp.networkandco.org.
155 IN PTR parefeu.networkandco.org.
```

/etc/resolv.conf
```
nameserver 192.168.40.151
search networkandco.org
```

# Service Mail

<p align="center"> <img src="img/Postfix_logo.png" alt="drawing" width="175"/>
</p>

Comme le cahier des charges le précisait, nous avons mis en place un serveur mail afin de permettre aux différents utilisateurs de communiquer efficacement, pour cela nous avons utilisé postfix comme serveur de transmission de courriers électroniques..
Il est bien répandu, fiable, libre et a de l'expérience.
Postfix est un serveur de messagerie STMP libre et répandu dans le domaine Linux. Postfix est assez simple dans son fonctionnement et sa configuration l'est aussi. Nous avons configuré le fichier **main.cf** pour l'adapter à nos besoins.

<p align="center"> <img src="img/serveur-mail.png" alt="drawing" width="500"/>
</p>

# Serveur Web

<p align="center"> <img src="img/Apache_logo.svg.png" alt="drawing" width="175"/>
</p>

Pour le serveur web nous avons décidé d'utiliser Apache car il est open-source et possède une communauté conséquente.
Voici la page web, à laquelle nous pouvons accèder via le DNS en rentrant *networkandco.org* .

Voici la configuration du site web */etc/apache2/sites-available/nethub-cgir.com.conf*:

```
<VirtualHost *:80>
ServerAdmin webmaster@networkandco.org
        DocumentRoot /var/www/html/
        ErrorLog ${APACHE_LOG_DIR}/error_networkandco.log
        CustomLog ${APACHE_LOG_DIR}/access_networkandco.log combined

        ServerName www.networkandco.org
        ServerAlias networkandco.org
        <Directory /var/www/html/>
                Options Indexes FollowSymLinks
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
```

Le fichier index.html se trouve dans le dossier */var/www/html*.


<p align="center"> <img src="img/site.png" alt="drawing" width="500"/>
</p>

# Conclusion

Pour conclure grâce à ce projet nous avons pu mettre en pratique des compétences déjà acquises en cours mais aussi à nous renseigner et former sur des sujets qui n'étaient pas dans le programme. 
Ce projet nous aura apporté des connaissances sur la mise en place d'un réseau en entreprise, ainsi que mettre en place une organisation afin de pouvoir optimiser notre temps au mieux pendant l'élaboration de ce projet.
Travailler en groupe est également une très bonne expérience entre le fait de mettre en commun nos compétences et de confronter nos idées pour aboutir à ce projet.