# Projet Network&Co

| Projet Network&Co                 |
|-----------------------------------|
| Création d'un Réseau d'Entreprise |
| Superviseur : Yvan Peter          |
| Étudiants : AL MASSATI Bilal, MARC Vianney, RENIER Antonin, LACHEREZ Hugo |

<p align="center"> <img src="docs/img/schema_projet.png" alt="drawing"/>
</p>

# Présentation du projet

Notre projet comporte comme demande principale l‘installation d’une infrastructure réseau d’entreprise, il est composé d’une demande spécifique comme :

* Il doit comporter une partie publique, accessible de l'extérieur et une partie privée pour les machines utilisateurs.
* Chaque groupe devra héberger son serveur web et celui-ci devra être accessible aux autres groupes.
* Les utilisateurs pourront s'échanger des mails y compris d'un groupe à l'autre.
* La partie privé sera sécurisée par un pc sous Linux.
* Les adresses de la partie privée seront en adressage privé et ne devront pas être visibles à l’extérieur.
* Les machines de la partie privée appartiennent soit au département comptabilité soit au département production en sachant que les flux de ces différents départements devront être séparés pour assurer la qualité de service du réseau. Toutefois, une communication entre les machines des deux départements doit rester possible.
* De manière à faciliter la vie de l’administrateur et des utilisateurs, les machines utilisateur seront configurées dynamiquement. 
* Les différentes machines utilisateurs ainsi que les serveurs pourront être désignés par un nom.

Notre tuteur de projet a prévue des tâches en plus, si nous arrivons à terme du projet en avance, notamment :
* Fournir l’ensemble des services en IPv4 et en IPv6
* Offrir un accès sécurisé depuis un réseau extérieur (VPN).
* Offrir aux utilisateurs un accès Wifi. Le flux correspondant sera séparé   des autres flux et permettra uniquement le flux HTTP.

# Remerciements

Nous tenons à d'abord remercier :

* **Yvan Peter** et **Michael Hauspie** : Pour nous avoir aidé et conseillé durant le projet.

* **L'IUT A de Lille** : Pour nous avoir fourni le matériel permettant la réalisation du projet.

# Structure de ce dépôt

* [`configs`](configs) contient les configurations routeurs, switch et certaines informations sur le réseau.
* [`docs`](docs) contient les fichiers de rapport et procédures.

# Détails sur le projet

Plan d'adressage IP :
```
VLAN 2 Administration : 192.168.40.2/24
VLAN 3 Comptabilité : 192.168.50.3/24
VLAN 4 Production : 192.168.60.4/24
```

* PC Admin. sous Debian : 192.168.10.1
* DHCP (isc-dhcp-server): 192.168.40.150
* DNS (bind9) : 192.168.40.151
* Web (Apache2) : 192.168.40.159
* Firewall (non-utilisé): 192.168.40.155
* Mail (Postfix) : 192.168.40.153
